# CashConnect

A WalletConnect implementation that uses CashRPC payloads and methods.

## Dapp Integration

TODO

## Wallet Integration

```ts
// Instantiate an instance of the Cash Connect Wallet Service.
const cashConnectWallet = new CashConnectWallet(
  // Master Private Key
  // NOTE: Each service will derive its own "sandboxed" key for use using:
  //       sha256(sha256(masterPrivateKey) + sha256(':cashconnect:' + domain));
  privateKey,
  // WC Project ID
  projectId,
  // WC Metadata
  metadata,
  // UI Event callbacks.
  // NOTE: Throw an exception to reject approval.
  {
    onSessionProposal: async (proposalEvent) => {
      // TODO: Display the proposal and request approval.
    },
    onSessionsUpdated: async (sessions) => {
      // TODO: Update UI to show active sessions.
    },
    onRPCRequest: async (session, request, response) => {
      // TODO: Display the request and request approval.
    },
    onError: async (error) => {
      // TODO: Display the error.
    }
  },
  // CashRPC Options/Hooks
  {
    // Network-related
    network: {
      getSourceOutput: (transactionHash, outpointIndex) {
        // TODO: Retrieve source output from Network (e.g. Electrum).

        // Expects LibAuth Output.
        return {
          valueSatoshis: 1000n,
          lockingBytecode: new Uint8Array(),
          token: {
            category: new Uint8Array();
            amount: 1000n,
          }
        }
      }
    },
    // Wallet-related
    wallet: {
      // Get a list of unspents (UTXOs) belonging to the wallet.
      getUnspents: () => {
        // TODO: Retrieve a list of UTXOs from Network or Wallet-state.

        // Return an array of these unspents.
        // NOTE: Each UTXO MUST use a template for the unlocking script.
        return [{
          outpointTransactionHash: utxo.txid,
          outpointIndex: utxo.vout,
          lockingBytecode: utxo.lockingBytecode,
          unlockingBytecode: {
            template: authenticationTemplateP2pkhNonHd,
            valueSatoshis: utxo.valueSatoshis,
            script: 'unlock',
            data: {
              keys: {
                privateKeys: {
                  key: walletsPrivateKey,
                },
              },
            },
            token,
          }
        }]
      },
      // Get the change template to when constructing Change Outputs.
      getChangeTemplate: () => {
        return {
          template: authenticationTemplateP2pkhNonHd,
          data: {
            keys: {
              privateKeys: {
                key: walletsPrivateKey
              }
            }
          }
        }
      }
    }
  }
);

// Start the Wallet Service.
await cashConnectWallet.start();
```

For an example Wallet Integration, see the [Cashonize Repository](https://github.com/developers-cash/cashonize/blob/cashconnect/cash-connect.js).
