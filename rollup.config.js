import pkg from './package.json' assert { type: 'json' };
import dts from 'rollup-plugin-dts';

export default [
  // CommonJS and ESM.
  {
    // Specify entry point for building
    input: 'build/lib/index.js',

    // Omit all dependencies and peer dependencies from the final bundle
    external: [
      ...Object.keys(pkg.dependencies || {}),
      ...Object.keys(pkg.peerDependencies || {}),
    ],

    // Produce outputs for ES and CJS
    output: [
      { file: pkg.exports.import, format: 'es' },
      { file: pkg.exports.require, format: 'cjs' },
    ],
  },
  // Build type definitions for the library
  {
    input: 'build/lib/index.d.ts',
    output: [{ file: pkg.typings, format: 'es' }],
    plugins: [dts()],
  },
];
