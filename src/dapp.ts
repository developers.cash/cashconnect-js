import type { RpcRequestResponse } from './rpc-types.js';
import { parseExtendedJson } from './utils.js';

import Client from '@walletconnect/sign-client';
import { generateNonce } from '@walletconnect/auth-client';
import type {
  ProposalTypes,
  SignClientTypes,
  SessionTypes,
} from '@walletconnect/types';

import {
  stringify,
  secp256k1,
  sha256,
  utf8ToBin,
  hexToBin,
} from '@bitauth/libauth';
import EventEmitter from 'events';
import { AuthRequestV0, CacaoAuth, CacaoPayload } from './types.js';

// NOTE: This design might feel a bit unintuitive but there are reasons for it.
//       WalletConnect uses a system where requests are dispatched to a relay and queued.
//       This introduces some difficult to handle edge-cases.
//       - Wallet and Dapp can fall out of sync.
//       - There is no (built-in) method to cancel requests.
//       - The JS Lib (at time of writing) does not always give sensible responses on error.
//       Thus, this approach is attempting to accommodate those edge-cases.
//       That said, it can probably be done better.

export interface RequestOpts {
  hide?: true;
  message?: string;
}

export type DialogCallback = () => void;

export class BaseNewSessionDialog {
  constructor(
    public readonly url: string,
    public readonly cancelCallback: DialogCallback
  ) {}

  cancel() {
    this.cancelCallback();
  }

  close() {
    // TODO: Hide the dialog.
    console.log('Close not implemented');
  }
}

export class BaseReconnectSessionDialog {
  constructor(
    public readonly cancelCallback: DialogCallback,
    public readonly newSessionCallback: DialogCallback
  ) {}

  cancel() {
    this.cancelCallback();
  }

  newConnection() {
    this.newSessionCallback();
  }

  close() {
    // TODO: Hide the dialog.
    console.log('Close not implemented');
  }
}

export class BaseRequestDialog {
  constructor(
    public opts: RequestOpts,
    public cancelCallback: DialogCallback
  ) {}

  cancel() {
    this.cancelCallback();
  }

  close() {
    // TODO: Hide the dialog.
    console.log('Close not implemented');
  }
}

export interface DappDialogs {
  newSession: typeof BaseNewSessionDialog;
  reconnectSession: typeof BaseReconnectSessionDialog;
  request: typeof BaseRequestDialog;
}

export class CashConnectDapp extends EventEmitter {
  client!: Client;
  session?: SessionTypes.Struct;

  // Callbacks
  onConnected?: (session: SessionTypes.Struct) => Promise<void>;

  constructor(
    public readonly options: SignClientTypes.Options,
    public readonly chain: string,
    public readonly dialogs: DappDialogs
  ) {
    super();
  }

  async start(): Promise<void> {
    // Initialize the WC client.
    this.client = await Client.init(this.options);

    // Set event callbacks.
    // ({ topic, params })
    this.client.on('session_update', this.onSessionUpdate.bind(this));
    this.client.on('session_delete', this.onSessionDisconnect.bind(this));
    this.client.on('session_event', this.onSessionEvent.bind(this));
  }

  async connect(
    requiredNamespaces: ProposalTypes.RequiredNamespaces
  ): Promise<boolean> {
    // If client has not been initialized, then initialize it.
    if (!this.client) {
      await this.start();
    }

    // Attempt to connect previous session.
    try {
      const reconnectResult = await this.reconnectSession(requiredNamespaces);

      if (reconnectResult === 'CONNECTED') {
        return true;
      } else if (reconnectResult === 'CANCELLED') {
        return false;
      }
    } catch (error: unknown) {
      console.error(error);
    }

    // Attempt to connect a new session.
    try {
      const newSessionResult = await this.newSession(requiredNamespaces);

      if (newSessionResult === 'CONNECTED') {
        return true;
      } else if (newSessionResult === 'CANCELLED') {
        return false;
      }
    } catch (error: unknown) {
      console.error(error);
    }

    // Return false.
    return false;
  }

  async newSession(requiredNamespaces: ProposalTypes.RequiredNamespaces) {
    const waitForResult = new Promise<'CONNECTED' | 'CANCELLED'>(
      async (resolve, reject) => {
        // We cannot cancel requests in WC.
        // So, if an action has already been taken, we just swallow the response.
        let swallowResponse = false;

        // Connect to the WC relay and get a connection URL.
        const { uri, approval } = await this.client.connect({
          requiredNamespaces,
        });

        // If no URI is available, thrown an error.
        if (!uri) {
          throw new Error('No URI available for some reason');
        }

        // Replace the "wc:" protocol handler with "cc:" (CashConnect).
        // NOTE: We do this to distinguish between other WC protocols.
        const uriCashConnect = uri.replace('wc:', 'cc:');

        // Create the dialog.
        const dialog = new this.dialogs.newSession(uriCashConnect, () => {
          swallowResponse = true;
          reject(new Error('New session cancelled'));
        });

        // Hook into approval event.
        approval()
          .then(async (session) => {
            // If the response has already been handled...
            if (swallowResponse) {
              return;
            }

            // Set the session.
            this.session = session;

            // Invoke our connection callback.
            if (this.onConnected) {
              await this.onConnected(session);
            }

            // Resolve with connected.
            resolve('CONNECTED');

            // Clean up inactive sessions.
            this.disconnectInactiveSessions();
          })
          .catch((error: unknown) => {
            // If the response has already been handled...
            if (swallowResponse) {
              return;
            }

            reject(error);
          })
          .finally(() => {
            // Close the dialog.
            dialog.close();
          });
      }
    );

    return waitForResult;
  }

  async reconnectSession(
    requiredNamespaces: ProposalTypes.RequiredNamespaces
  ): Promise<'CONNECTED' | 'CANCELLED' | 'NEW_SESSION'> {
    // Attempt to get the previous session.
    const lastKeyIndex = this.client.session.getAll().length - 1;
    const session = this.client.session.getAll()[lastKeyIndex];

    if (!session) {
      return 'NEW_SESSION';
    }

    // Compare the namespaces of the previous session with the requested namespaces.
    const prevSessionNamespaces = JSON.stringify(session.requiredNamespaces);
    const requestedSessionNamespaces = JSON.stringify(requiredNamespaces);

    // If the previous session DOES NOT match the requested, invoke new session.
    if (prevSessionNamespaces !== requestedSessionNamespaces) {
      return 'NEW_SESSION';
    }

    const waitForResult = new Promise<
      'CONNECTED' | 'CANCELLED' | 'NEW_SESSION'
    >((resolve, reject) => {
      // We cannot cancel requests in WC.
      // So, if an action has already been taken, we just swallow the response.
      let swallowResponse = false;

      // Create the dialog.
      const dialog = new this.dialogs.reconnectSession(
        () => {
          swallowResponse = true;
          resolve('CANCELLED');
        },
        () => {
          swallowResponse = true;
          resolve('NEW_SESSION');
        }
      );

      // Attempt to ping the wallet.
      this.client
        .ping({ topic: session.topic })
        .then(async () => {
          // If the response has already been handled...
          if (swallowResponse) {
            return;
          }

          // Set our session.
          this.session = session;

          // Invoke our connection callback.
          if (this.onConnected) {
            await this.onConnected(session);
          }

          // Resolve as connected.
          resolve('CONNECTED');

          // Clean up inactive sessions.
          this.disconnectInactiveSessions();
        })
        .catch((error: unknown) => {
          // If the response has already been handled...
          if (swallowResponse) {
            return;
          }

          reject(error);
        })
        .finally(() => {
          dialog.close();
        });
    });

    return waitForResult;
  }

  async disconnect(): Promise<void> {
    if (!this.client || !this.session) {
      return;
    }

    // Disconnect this session.
    await this.client.disconnect({
      topic: this.session.topic,
      reason: {
        code: 1000,
        message: 'Disconnected',
      },
    });

    // Disconnect any remaining sessions.
    // NOTE: We do this because WalletConnect can easily desync.
    //       So, let's make sure we clear up any stagnant sessions.
    this.client.session.getAll().forEach((session) => {
      this.client.session.delete(session.topic, {
        code: 1000,
        message: 'Disconnected',
      });
    });

    this.onSessionDisconnect();
  }

  async request<T extends RpcRequestResponse>(
    method: T['request']['method'],
    params: T['request']['params'] = undefined,
    opts: RequestOpts = {}
  ): Promise<T['response']> {
    if (!this.client) {
      throw new Error('Client is not initialized');
    }

    if (!this.session) {
      throw new Error('Session is not initialized');
    }

    const waitForResult = new Promise<T['response'] | 'CANCELLED'>(
      (resolve, reject) => {
        // We cannot cancel requests in WC.
        // So, if an action has already been taken, we just swallow the response.
        let swallowResponse = false;

        // Create the dialog.
        const dialog = new this.dialogs.request(opts, () => {
          swallowResponse = true;
          reject(new Error('Request cancelled'));
        });

        this.client
          .request<string>({
            chainId: this.chain,
            topic: this.session!.topic,
            request: {
              method,
              params: stringify(params),
            },
          })
          .then((result) => {
            // If the response has already been handled...
            if (swallowResponse) {
              return;
            }

            resolve(parseExtendedJson(result));
          })
          .catch((error: unknown) => {
            // If the response has already been handled...
            if (swallowResponse) {
              return;
            }

            reject(error);
          })
          .finally(() => {
            // Close the dialog.
            dialog.close();
          });
      }
    );

    return waitForResult;
  }

  async authRequest(opts: RequestOpts): Promise<CacaoAuth> {
    if (!this.session) {
      throw new Error('No session currently active');
    }

    // Build the CacaoPayload.
    const cacaoPayload: CacaoPayload = {
      aud: this.options.metadata?.url || '',
      domain: this.options.metadata?.url || '',
      chainId: this.chain,
      type: 'cashconnect',
      nonce: generateNonce(),
      iat: new Date().toISOString(),
    };

    // Request wallet to sign the Cacao Payload.
    const signature = await this.request<AuthRequestV0>(
      'wc_authRequest',
      {
        requester: this.session.self,
        cacaoPayload,
      },
      opts
    );

    // Retrieve the public key that we acquired when session was established.
    // NOTE: Split format: ${chain}:${network}:pubkey
    const publicKey = hexToBin(
      this.session.namespaces.bch?.accounts[0].split(':')[2]
    );

    // Stringify the cacao payload we sent.
    const cacaoPayloadStringified = JSON.stringify(cacaoPayload);

    // Hash it.
    const cacaoPayloadDigest = sha256.hash(utf8ToBin(cacaoPayloadStringified));

    // Verify the signature.
    if (
      !secp256k1.verifySignatureSchnorr(
        signature,
        publicKey,
        cacaoPayloadDigest
      )
    ) {
      throw new Error('Signature verification failed');
    }

    return {
      cacaoPayload,
      publicKey,
      signature,
    };
  }

  async disconnectInactiveSessions() {
    // Do some house-cleaning by removing all other sessions that may exist.
    // NOTE: We do this because WalletConnect can easily desync.
    //       So, let's make sure we clear up any stagnant sessions.
    this.client.session.getAll().forEach((session) => {
      // DO NOT remove the current session.
      if (session.topic === this.session?.topic) {
        return;
      }

      // Delete the session.
      this.client.session.delete(session.topic, {
        code: 1000,
        message: 'Disconnected',
      });
    });
  }

  async onSessionDisconnect() {
    this.emit('disconnected');
  }

  async onSessionUpdate() {
    this.emit('updated');
  }

  async onSessionEvent(event: any) {
    this.emit('sessionEvent', event);
  }
}
