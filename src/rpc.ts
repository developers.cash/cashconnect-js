// NOTE: The RPC's here are intended to be transport-agnostic.
//       They will likely be split out into their own library for use with a Payment Protocol over HTTP (or LibP2P).
//       Tentative name for this is CashRPC.
//       It may also find use WITHIN wallets.

// TODO: Consider using composable pattern.
//       There's different use-cases for this, so it might be worth composing a class for each use-case.

import type {
  CompilationDataBCH,
  InputTemplateBCH,
  OutputTemplateBCH,
  GetChangeLockingBytecodeV0,
  SignTransactionV0,
  GetBalanceV0,
  SignTransactionV0InputTemplateScoped,
  SignTransactionV0InputWalletScoped,
  SignTransactionV0OutputTemplateScoped,
  SignTransactionV0Data,
  GetTokensV0,
  SignTransactionV0OutputWalletScoped,
  SignTransactionV0Response,
  SourceOutputs,
  Unspent,
  SignMessageV0,
  CashRPCV0Opts,
} from './rpc-types';
import { CashRPCError } from './rpc-types.js';
import { TransactionBuilder } from './transaction';

import { getValueByPath } from './utils';

import {
  stringifyDebugTraceSummary,
  summarizeDebugTrace,
  binsAreEqual,
  binToHex,
  createVirtualMachineBCH,
  importWalletTemplate,
  sha256,
  secp256k1,
  decodeTransactionBCH,
  walletTemplateToCompilerBCH,
} from '@bitauth/libauth';
import type { WalletTemplate, CompilerBCH, Output } from '@bitauth/libauth';

export class CashRPC_V0 {
  public readonly template?: WalletTemplate;
  public readonly templateCompiler?: CompilerBCH;

  constructor(public readonly opts: CashRPCV0Opts) {
    if (opts.scope?.template) {
      // Import the provided template.
      const importedTemplate = importWalletTemplate(opts.scope.template);

      // Ensure that parsing was successful.
      if (typeof importedTemplate === 'string') {
        throw new Error(
          `Failed to parse the provided template: ${opts.scope.template}`
        );
      }

      // Save the template.
      this.template = importedTemplate;

      // Create a compiler for this template.
      this.templateCompiler = walletTemplateToCompilerBCH(this.template);
    }
  }

  //-----------------------------------------------------------------------------
  // RPC Methods
  //-----------------------------------------------------------------------------

  async getBalance(): Promise<GetBalanceV0['response']> {
    if (!this.opts.wallet) {
      throw new Error(
        'getUnspents is not supported: CashRPC not configured with Wallet.'
      );
    }

    // Get a list of unspents from the wallet.
    const unspents = await this.opts.wallet.getUnspents();

    // Create a map to store our balances.
    const balances: { [id: string]: bigint } = { bch: 0n };

    // Tally up the totals for BCH.
    // NOTE: We do not include sats for unspents that have a token attached.
    //       This is consistent with how UTXOs are selected for BCH amounts.
    //       (Which is done to adhere to Contracts that constrain number of outputs...)
    //       (Otherwise, we would need to add "change" for the input containing a token...)
    //       (Which would likely cause the "outputs.length" constraint to fail).
    balances.bch = unspents.reduce((total, unspent) => {
      // If this unspent contains a token, do not include in our totals.
      if (unspent.unlockingBytecode.token) {
        return total;
      }

      // Append the sats to our total.
      return (total += unspent.unlockingBytecode.valueSatoshis);
    }, 0n);

    // Filter for those that a) contain tokens and b) are in our list of allowed token category IDs.
    const allowedUnspents = unspents.filter(
      this.doesUnspentContainAllowedToken.bind(this)
    );

    // Add to the balances for each of our whitelisted tokens.
    allowedUnspents.forEach((tokenUnspent) => {
      const categoryId = binToHex(
        tokenUnspent.unlockingBytecode.token!.category
      );
      const amount = tokenUnspent.unlockingBytecode.token!.amount;

      const currentTokenTotal = balances[categoryId] || 0n;
      const newTotal = currentTokenTotal + amount;
      balances[categoryId] = newTotal;
    });

    return balances;
  }

  async getChangeLockingBytecode(): Promise<
    GetChangeLockingBytecodeV0['response']
  > {
    if (!this.opts.wallet) {
      throw new Error(
        'getChangeLockingBytecode is not supported: CashRPC not configured with Wallet.'
      );
    }

    // Get the change output from the wallet.
    const changeTemplate = await this.opts.wallet.getChangeTemplate();

    // Compile the change template.
    const compiler = walletTemplateToCompilerBCH(changeTemplate.template);

    // Compile the locking bytecode.
    // TODO: Make scriptId adjustable.
    const changeLockingBytecode = compiler.generateBytecode({
      data: changeTemplate.data,
      scriptId: 'lock',
    });

    // If script compilatino failed, throw an error.
    if (!changeLockingBytecode.success) {
      throw new Error('Failed to compile change script "lock"');
    }

    // Return the bytecode.
    return changeLockingBytecode.bytecode;
  }

  async getTokens(): Promise<GetTokensV0['response']> {
    if (!this.opts.wallet) {
      throw new Error(
        'getUnspents is not supported: CashRPC not configured with Wallet.'
      );
    }

    if (!this.opts.scope?.allowedTokens) {
      throw new Error(
        'Failed to get tokens: scope.allowedTokens not specified.'
      );
    }

    // Get all unspents.
    const unspents = await this.opts.wallet.getUnspents();

    // Filter for those that a) contain tokens and b) are in our list of allowed token category IDs.
    const allowedUnspents = unspents.filter(
      this.doesUnspentContainAllowedToken.bind(this)
    );

    // Turn these unspents into an input.
    const unspentsAsInputs = allowedUnspents.map((unspent) => ({
      outpointTransactionHash: unspent.outpointTransactionHash,
      outpointIndex: unspent.outpointIndex,
      token: {
        category: unspent.unlockingBytecode.token!.category,
        amount: unspent.unlockingBytecode.token!.amount,
        nft: unspent.unlockingBytecode.token?.nft,
      },
    }));

    return unspentsAsInputs;
  }

  async signMessage(
    params: SignMessageV0['request']['params']
  ): Promise<SignMessageV0['response']> {
    if (!this.opts.signerKey) {
      throw new Error(
        'signMessage is not supported: CashRPC not configured with SignerKey.'
      );
    }

    // Check if payload looks malicious.
    if (this.doesPayloadLookMalicious(params.message)) {
      throw new Error('Failed to sign message: Payload looks malicious');
    }

    // SHA256 hash the message to get a digest.
    const messageDigest = sha256.hash(params.message);

    // Attempt to sign the message.
    const signResult = secp256k1.signMessageHashSchnorr(
      this.opts.signerKey,
      messageDigest
    );

    // If a string is returned, this indicates an error...
    if (typeof signResult === 'string') {
      throw new Error(signResult);
    }

    return signResult;
  }

  async signTransaction(
    params: SignTransactionV0['request']['params']
  ): Promise<SignTransactionV0['response']> {
    if (!this.template) {
      throw new Error('Failed to sign transaction: No template provided');
    }

    // Store a list of results.
    const resultStack: Array<SignTransactionV0Response> = [];

    // Get a list of wallet UTXOs and declare a variable to store our sourceOutputs.
    const unspents = this.opts.wallet
      ? await this.opts.wallet.getUnspents()
      : [];

    // Get the change template from the wallet.
    const changeTemplate = this.opts.wallet
      ? await this.opts.wallet.getChangeTemplate()
      : undefined;

    // Declare a variable to store any new source outputs that get created.
    const sourceOutputs: SourceOutputs = {};

    // Iterate through each transaction.
    for (const [txIndex, tx] of params.entries()) {
      // Populate any input placeholders with the result of previous payloads.
      if (tx.transaction.inputs) {
        tx.transaction.inputs.forEach((input) => {
          if (typeof input.outpointTransactionHash === 'string') {
            input.outpointTransactionHash = getValueByPath(
              resultStack,
              input.outpointTransactionHash
            );
          }
        });
      }

      // Populate any output (and data) placeholders with the result of previous payloads.
      if (tx.transaction.outputs) {
        tx.transaction.outputs.forEach((output) => {
          if (typeof output.token?.category === 'string') {
            output.token.category = getValueByPath(
              resultStack,
              output.token.category
            );
          }

          if ('script' in output) {
            for (const [key, value] of Object.entries(output.data || {})) {
              if (typeof value === 'string') {
                output.data![key] = getValueByPath(resultStack, value);
              }
            }
          }
        });
      }

      // Transform the transaction so that it contains the compiler from template and data.
      const transactionBuilder = new TransactionBuilder({
        version: tx.transaction.version || 2,
        locktime: tx.transaction.locktime || 0,
        inputs: await this.parseProvidedInputs(
          tx.transaction.inputs || [],
          unspents,
          sourceOutputs,
          tx.signerKey
        ),
        outputs: await this.parseProvidedOutputs(
          tx.transaction.outputs || [],
          tx.signerKey
        ),
      });

      // Populate the transaction with unspents from our wallet.
      // NOTE: We only do this if a getUnspents hook is provided.
      if (this.opts.wallet && changeTemplate) {
        transactionBuilder
          .usingUnspents(unspents)
          .addTokensFromUnspents()
          .addTokenChangeOutputs(changeTemplate)
          .addSatoshisFromUnspents()
          .addSatoshiChangeOutput(changeTemplate);
      }

      // Generate the transaction and get the hash.
      const generatedTx = transactionBuilder.generate();
      const generatedTxHash = transactionBuilder.hash();

      console.log(generatedTx);

      // Simulate the transaction by creating a VM.
      // TODO: I might not be using this correctly.
      //       I don't think it will properly compare against the lockscript?
      if (tx.validate !== false) {
        const virtualMachine = createVirtualMachineBCH();
        const virtualMachineResult = virtualMachine.verify(generatedTx);

        // If simulation of the transaction fails...
        if (typeof virtualMachineResult === 'string') {
          // Declare a variable to hold our stack trace.
          let stackTrace = `Transaction #${txIndex} Stack Trace\n`;

          // Iterate over each input and generate detailed debugging info.
          for (const [inputIndex] of generatedTx.transaction.inputs.entries()) {
            // Debug this input
            const state = virtualMachine.debug({
              inputIndex,
              sourceOutputs: generatedTx.sourceOutputs,
              transaction: generatedTx.transaction,
            });

            // Summarize and stringify the result.
            const summarizedDebugTrace = summarizeDebugTrace(state);
            const stringifiedDebugTraceSummary =
              stringifyDebugTraceSummary(summarizedDebugTrace);

            // Append the trace to our stack.
            stackTrace += `Input #${inputIndex}\n`;
            stackTrace += `${stringifiedDebugTraceSummary}`;
          }

          throw new CashRPCError(
            `Transaction #${txIndex}: ${virtualMachineResult}`,
            stackTrace
          );
        }
      }

      // Remove any of the wallet unspents that were used.
      for (const input of generatedTx.transaction.inputs) {
        // Try to find this input in our list of unspents.
        const unspentIndex = unspents.findIndex(
          (unspent) =>
            binsAreEqual(
              input.outpointTransactionHash,
              unspent.outpointTransactionHash
            ) && input.outpointIndex === unspent.outpointIndex
        );

        // If we found it, remove it.
        if (unspentIndex !== -1) {
          unspents.splice(unspentIndex, 1);
        }
      }

      // Add the change outputs back to our unspents so that we can use them for future tx's.
      for (const changeOutput of transactionBuilder.change) {
        changeOutput.outpointTransactionHash = generatedTxHash;
        unspents.push(changeOutput);
      }

      // Add any outputs to the list of our source outputs.
      generatedTx.transaction.outputs.forEach((output, i) => {
        sourceOutputs[`${generatedTxHash}:${i}`] = output;
      });

      // Push the result of this payload onto our stack.
      resultStack.push({
        transactionHash: generatedTxHash,
        transaction: generatedTx.transaction,
        sourceOutputs: generatedTx.sourceOutputs,
      });
    }

    // Return the result of this call.
    return resultStack;
  }

  //-----------------------------------------------------------------------------
  // Utilities
  //-----------------------------------------------------------------------------

  async provideData(
    signerKey?: string,
    data: SignTransactionV0Data = {}
  ): Promise<CompilationDataBCH> {
    const providedData: CompilationDataBCH = {
      bytecode: {
        ...data,
      },
      keys: {
        privateKeys: {},
      },
    };

    // If a wallet object was provided, add the change locking bytecode to the data.
    if (this.opts.wallet) {
      providedData.bytecode = {
        ...providedData.bytecode,
        changeLockingBytecode: await this.getChangeLockingBytecode(),
      };
    }

    // If a signer key was provided, add it to the data.
    if (this.opts.signerKey && signerKey) {
      providedData.keys = {
        privateKeys: {
          [signerKey]: this.opts.signerKey,
        },
      };
    }

    return providedData;
  }

  async parseProvidedInputs(
    inputs: Array<
      SignTransactionV0InputTemplateScoped | SignTransactionV0InputWalletScoped
    >,
    unspents: Array<Unspent>,
    localSourceOutputs: SourceOutputs,
    signerKey?: string
  ): Promise<Array<InputTemplateBCH>> {
    // Declare an array to store our transformed inputs.
    const transformedInputs: Array<InputTemplateBCH> = [];

    // Transform our inputs into LibAuth Template format.
    for (const input of inputs) {
      // If this is an input provided by the template
      if ('script' in input) {
        transformedInputs.push({
          outpointTransactionHash: input.outpointTransactionHash,
          outpointIndex: input.outpointIndex,
          sequenceNumber: 0,
          unlockingBytecode: {
            compiler: this.templateCompiler!,
            script: input.script,
            data: await this.provideData(signerKey, input.data),
            // TODO: Probably want to do this in parallel to speed things up.
            // TODO: This needs to be typed! Surprised Typescript does not complain.
            ...(await this.getSourceOutput(
              input.outpointTransactionHash,
              input.outpointIndex,
              localSourceOutputs
            )),
          },
        });
      }
      // Otherwise, this is an input provided by the wallet.
      else {
        // Find the corresponding unspent's index.
        const unspentIndex = unspents.findIndex(
          (unspent) =>
            unspent.outpointTransactionHash === input.outpointTransactionHash &&
            unspent.outpointIndex === input.outpointIndex
        );

        // If the unspent could not be found...
        if (unspentIndex === -1) {
          throw new Error(
            `Input ${binToHex(input.outpointTransactionHash)}:${
              input.outpointIndex
            } does not exist in wallet.`
          );
        }

        // Assign the unspent to a variable
        const unspent = unspents[unspentIndex];

        // Remove the unspent from our list of unspents (so we don't attempt to re-use it later).
        unspents.splice(unspentIndex, 1);

        // Check if unspent allowed to be used.
        if (!this.doesUnspentContainAllowedToken(unspent)) {
          throw new Error(
            `Input ${binToHex(input.outpointTransactionHash)}:${
              input.outpointIndex
            } does not contain an allowed Category ID.`
          );
        }

        // Create a compiler for this input.
        const compiler = walletTemplateToCompilerBCH(
          unspent.unlockingBytecode.template
        );

        // Transform the input to Libauth Format.
        transformedInputs.push({
          outpointTransactionHash: input.outpointTransactionHash,
          outpointIndex: input.outpointIndex,
          sequenceNumber: 0,
          unlockingBytecode: { ...unspent.unlockingBytecode, compiler },
        });
      }
    }

    // Return them.
    return transformedInputs;
  }

  async parseProvidedOutputs(
    outputs: Array<
      | SignTransactionV0OutputTemplateScoped
      | SignTransactionV0OutputWalletScoped
    >,
    signerKey?: string
  ): Promise<Array<OutputTemplateBCH>> {
    // Declare an array to store our transformed outputs.
    const transformedOutputs: Array<OutputTemplateBCH> = [];

    // Transform our outputs into LibAuth Template format.
    for (const output of outputs) {
      // If this is an output being sent to the Sandbox
      if ('script' in output) {
        transformedOutputs.push({
          valueSatoshis: output.valueSatoshis,
          token: output.token,
          lockingBytecode: {
            compiler: this.templateCompiler!,
            script: output.script,
            data: await this.provideData(signerKey, output.data),
          },
        });
      }
      // Otherwise, this is an output being sent back to wallet.
      else {
        transformedOutputs.push({
          valueSatoshis: output.valueSatoshis,
          token: output.token,
          lockingBytecode: await this.getChangeLockingBytecode(),
        });
      }
    }

    // Return them.
    return transformedOutputs;
  }

  async getSourceOutput(
    outpointTransactionHash: Uint8Array,
    outpointIndex: number,
    localSourceOutputs: SourceOutputs
  ): Promise<Output> {
    const localSourceOutput =
      localSourceOutputs[`${outpointTransactionHash}:${outpointIndex}`];

    if (localSourceOutput) {
      return localSourceOutput;
    }

    // Otherwise, get the source output from the network.
    return await this.opts.network.getSourceOutput(
      outpointTransactionHash,
      outpointIndex
    );
  }

  doesUnspentContainAllowedToken(unspent: Unspent): boolean {
    if (!this.opts.scope?.allowedTokens) {
      return false;
    }

    if (!unspent.unlockingBytecode.token) {
      return false;
    }

    if (
      !this.opts.scope.allowedTokens.includes('*') &&
      !this.opts.scope.allowedTokens.includes(
        binToHex(unspent.unlockingBytecode.token.category)
      )
    ) {
      return false;
    }

    return true;
  }

  doesPayloadLookMalicious(payload: Uint8Array): boolean {
    // If the payload could be decoded as a BCH transaction, mark it as malicious.
    try {
      console.log(payload);
      const decodeResult = decodeTransactionBCH(payload);
      console.log(decodeResult);
      return typeof decodeResult !== 'string';
    } catch (error: unknown) {
      return false;
    }
  }
}
