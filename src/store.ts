import { IKeyValueStorage } from '@walletconnect/keyvaluestorage';
import { safeJsonStringify, safeJsonParse } from '@walletconnect/safe-json';

import { IDBPDatabase, openDB, deleteDB } from 'idb';

export class IndexedDbStore implements IKeyValueStorage {
  public db: Promise<IDBPDatabase>;

  constructor(name: string, public readonly storeName = 'keyval') {
    this.db = openDB(name, 1, {
      upgrade: (db) => {
        db.createObjectStore(storeName);
      },
    });
  }

  async waitForStart() {
    return await this.db;
  }

  async getItem<T = any>(key: string): Promise<T | undefined> {
    const item = await (await this.db).get(this.storeName, key);
    if (item === null || item === undefined) {
      return undefined;
    }
    return safeJsonParse(item) as T;
  }
  async setItem<T = any>(key: string, value: T): Promise<void> {
    (await this.db).put(this.storeName, safeJsonStringify(value), key);
  }
  async removeItem(key: string) {
    (await this.db).delete(this.storeName, key);
  }
  async clear() {
    (await this.db).clear(this.storeName);
  }
  async getKeys() {
    return (await this.db).getAllKeys<string>(this.storeName) as Promise<
      Array<string>
    >;
  }
  async getEntries() {
    const items = await (await this.db).getAll(this.storeName);
    return items.map(IndexedDbStore.parseEntry);
  }

  public static parseEntry(entry: [string, string | null]): [string, any] {
    return [entry[0], safeJsonParse(entry[1] ?? '')];
  }

  public static async deleteDb(name: string) {
    await deleteDB(name, {
      blocked() {},
    });
  }
}
