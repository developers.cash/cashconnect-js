import type { ChangeTemplate, Unspent } from './rpc-types.js';

import type {
  TransactionTemplateFixed,
  CompilerBCH,
  Output,
  ResolvedTransactionBCH,
} from '@bitauth/libauth';

import {
  encodeTransactionBCH,
  encodeTransactionOutput,
  getDustThreshold,
  hashTransactionUiOrder,
  binToHex,
  hexToBin,
  binsAreEqual,
  walletTemplateToCompilerBCH,
} from '@bitauth/libauth';

import { generateTransaction } from './generateTransaction.js';

// NOTE: DO NOT pluck this code for your own use yet.
//       It is not well-tested and is a mess.
//       Clean up will happen once I've fleshed it out a bit better.

// Mint Handling
// 1. Rename useUnspents to usingUnspents
// 2. Sort unspents from highest to lowest
// 2. Rename class to TransactionBuilder (maybe inherit from transaction)
// 3. Mint unpsnets can be a separaate function or included in the token calculations *unsure yet*
// 4. You will need to ignore Minting NFT's when calculating required tokens.
// 6. Consider whether this should be under Wallet as opposed to primitives.
// 7. The design of Wallet should be that there are various kinds:
//    Template Wallets (e.g. P2PKH Template)
//    HDWallet
//    These wallets will then inherit a common interface.
//    getUnspents
//    getBalance
//    etc

// TODO: UTXO selection here is not the most optimal.
//       We really want to do a cost/benefit analysis for each UTXO.
//       i.e. Calculate how much fee a given UTXO incurs.
//            And then deduct that fee from the satoshiValue.
//            And then sort UTXOs by the satoshiValue - feeIncurred.

/**
 * Transaction Entity.
 */
export class TransactionBuilder {
  constructor(
    public readonly transaction: TransactionTemplateFixed<CompilerBCH>,
    public unspents: Array<Unspent> = []
  ) {
    transaction.inputs.forEach((input) => {
      // TODO: Type all of this shit.
      if (input.unlockingBytecode instanceof Uint8Array) {
        throw new Error('Something something Uint8array not allowed, etc');
      }

      const sourceOutput = input.unlockingBytecode as any;

      this.sourceOutputs.push({
        lockingBytecode: sourceOutput.lockingBytecode,
        valueSatoshis: sourceOutput.valueSatoshis,
        token: sourceOutput.token,
      });
    });
  }
  public change: Array<Unspent> = [];
  public sourceOutputs: Array<Output<Uint8Array, Uint8Array>> = [];

  public calculateInputSats(): bigint {
    return this.transaction.inputs.reduce((inputSats, input, i) => {
      // Make sure the input has a "valueSatoshis" amount.
      if (input.unlockingBytecode instanceof Uint8Array) {
        throw new Error(
          `Failed to calculate input sats: Input #${i} does not have a satoshi value (valueSatoshis)`
        );
      }

      return (inputSats += input.unlockingBytecode.valueSatoshis);
    }, 0n);
  }

  public calculateInputTokens(): Map<string, bigint> {
    // NOTE: We must use a Map (as opposed to object) if we're indexing by Uint8Array.
    const tokensRequired: Map<string, bigint> = new Map();

    this.transaction.inputs.forEach((input, i) => {
      if (input.unlockingBytecode instanceof Uint8Array) {
        throw new Error(
          `Failed to calculate input token amount: Input #${i} is raw unlockingBytecode - must be an object`
        );
      }

      // If this input requires fungible tokens...
      if (input.unlockingBytecode.token && !input.unlockingBytecode.token.nft) {
        const currentTokenTotal =
          tokensRequired.get(
            binToHex(input.unlockingBytecode.token.category)
          ) || 0n;
        const newTotal =
          currentTokenTotal + input.unlockingBytecode.token.amount;
        tokensRequired.set(
          binToHex(input.unlockingBytecode.token.category),
          newTotal
        );
      }
    });

    return tokensRequired;
  }

  public calculateOutputSats(): bigint {
    return this.transaction.outputs.reduce(
      (outputTotal, output) => (outputTotal += output.valueSatoshis),
      0n
    );
  }

  public calculateOutputTokens(): Map<string, bigint> {
    // NOTE: We must use a Map (as opposed to object) if we're indexing by Uint8Array.
    const tokensRequired: Map<string, bigint> = new Map();

    this.transaction.outputs.forEach((output) => {
      // If this output has a token and it is not an NFT...
      if (output.token && !output.token.nft) {
        const currentTokenTotal =
          tokensRequired.get(binToHex(output.token.category)) || 0n;
        const newTotal = currentTokenTotal + output.token.amount;
        tokensRequired.set(binToHex(output.token.category), newTotal);
      }
    });

    return tokensRequired;
  }

  public calculateFeeSats(): bigint {
    return BigInt(this.toBytes().length);
  }

  public calculateRequiredSats(): bigint {
    const outputSats = this.calculateOutputSats();
    const feeSats = this.calculateFeeSats();
    const requiredSats = feeSats + outputSats;

    return requiredSats;
  }

  public usingUnspents(unspents: Array<Unspent>) {
    this.unspents = unspents;

    return this;
  }

  public addTokensFromUnspents(): TransactionBuilder {
    // Calculate the tokens required for this transaction.
    const tokensRequired = this.calculateOutputTokens();

    // Interate through each token and add inputs to meet the required amount.
    tokensRequired.forEach((requiredAmountForToken, categoryId) => {
      // Find unspents containing this category ID.
      const unspentsContainingToken = this.unspents.filter((unspent) =>
        binsAreEqual(
          unspent.unlockingBytecode.token?.category || new Uint8Array(),
          hexToBin(categoryId)
        )
      );

      // Sort them so that we prioritize the largest amounts first.
      const unspentsContainingTokenSorted = unspentsContainingToken.sort(
        (a, b) =>
          Number(b.unlockingBytecode.token?.amount || 0) -
          Number(a.unlockingBytecode.token?.amount || 0)
      );

      // Loop until we have enough input tokens to match the required amount of output tokens.
      while (
        (this.calculateInputTokens().get(categoryId) || 0n) <
        requiredAmountForToken
      ) {
        // Get the unspent to add from the top of our list and remove it.
        const unspent = unspentsContainingTokenSorted.shift();

        if (!unspent) {
          throw new Error(
            `Failed to select UTXOs: ${
              this.calculateInputTokens().get(categoryId) || 0n
            } of token (${categoryId}) available, ${requiredAmountForToken} required`
          );
        }

        // Create a compiler for this input.
        const compiler = walletTemplateToCompilerBCH(
          unspent.unlockingBytecode.template
        );

        // Append this Input.
        this.transaction.inputs.push({
          ...unspent,
          unlockingBytecode: { ...unspent.unlockingBytecode, compiler },
        });

        // Append to our list of sourceOuputs.
        this.sourceOutputs.push({
          lockingBytecode: unspent.lockingBytecode,
          valueSatoshis: unspent.unlockingBytecode.valueSatoshis,
          token: unspent.unlockingBytecode.token,
        });
      }
    });

    return this;
  }

  public addSatoshisFromUnspents(): TransactionBuilder {
    // Find unspents without tokens.
    const unspentsWithoutTokens = this.unspents.filter(
      (unspent) => typeof unspent.unlockingBytecode.token === 'undefined'
    );

    // Sort them so that we prioritize the largest amounts first.
    const unspentsWithoutTokensSorted = unspentsWithoutTokens.sort((a, b) =>
      Number(
        b.unlockingBytecode.valueSatoshis - a.unlockingBytecode.valueSatoshis
      )
    );

    // Iterate over each unspent until we get the amount desired.
    while (
      this.calculateInputSats() <
      this.calculateOutputSats() + this.calculateFeeSats()
    ) {
      // Get the unspent to add from the top of our list and remove it.
      const unspent = unspentsWithoutTokensSorted.shift();

      if (!unspent) {
        throw new Error(
          `Failed to select UTXOs: ${this.calculateInputSats()} of sats available, ${this.calculateRequiredSats()} required`
        );
      }

      // Create a compiler for this input.
      const compiler = walletTemplateToCompilerBCH(
        unspent.unlockingBytecode.template
      );

      // Append this unspent.
      this.transaction.inputs.push({
        ...unspent,
        sequenceNumber: 0,
        unlockingBytecode: { ...unspent.unlockingBytecode, compiler },
      });

      // Append to our list of sourceOuputs.
      this.sourceOutputs.push({
        lockingBytecode: unspent.lockingBytecode,
        valueSatoshis: unspent.unlockingBytecode.valueSatoshis,
        token: unspent.unlockingBytecode.token,
      });
    }

    return this;
  }

  public addTokenChangeOutputs(
    changeTemplate: ChangeTemplate
  ): TransactionBuilder {
    const compiler = walletTemplateToCompilerBCH(changeTemplate.template);

    const changeLockingBytecode = compiler.generateBytecode({
      data: changeTemplate.data,
      scriptId: 'lock',
    });

    if (!changeLockingBytecode.success) {
      throw new Error('Failed to compile change script "lock"');
    }

    const inputTokens = this.calculateInputTokens();
    const outputTokens = this.calculateOutputTokens();

    inputTokens.forEach((amount, categoryId) => {
      const inputAmount = amount;
      const outputAmount = outputTokens.get(categoryId) || 0n;
      const changeAmount = inputAmount - outputAmount;

      // Add the change output.
      this.transaction.outputs.push({
        lockingBytecode: changeLockingBytecode.bytecode,
        valueSatoshis: 1000n,
        token: {
          category: hexToBin(categoryId),
          amount: changeAmount,
        },
      });

      // And append it back to our UTXOs.
      this.change.push({
        outpointTransactionHash: new Uint8Array(),
        outpointIndex: this.transaction.outputs.length - 1,
        sequenceNumber: 0,
        lockingBytecode: changeLockingBytecode.bytecode,
        unlockingBytecode: {
          ...changeTemplate,
          valueSatoshis: 1000n,
          script: 'unlock',
          token: {
            amount: changeAmount,
            category: hexToBin(categoryId),
          },
        },
      });
    });

    return this;
  }

  public addSatoshiChangeOutput(
    changeTemplate: ChangeTemplate
  ): TransactionBuilder {
    const compiler = walletTemplateToCompilerBCH(changeTemplate.template);

    const changeLockingBytecode = compiler.generateBytecode({
      data: changeTemplate.data,
      scriptId: 'lock',
    });

    if (!changeLockingBytecode.success) {
      throw new Error('Failed to compile change script "lock"');
    }

    // NOTE: This might lead to over-paying the tx fee slightly.
    const changeOutputTemplate = {
      lockingBytecode: changeLockingBytecode.bytecode,
      valueSatoshis: 0n,
    };
    const changeOutputFee =
      BigInt(encodeTransactionOutput(changeOutputTemplate).length) + 4n;

    // Calculate the input satoshis and the current transaction fee.
    const inputSats = this.calculateInputSats();
    const outputSats = this.calculateOutputSats();
    const transactionFee = this.calculateFeeSats() + changeOutputFee;

    // Calculate the change satoshis.
    const changeSats = inputSats - (outputSats + transactionFee);

    // If the Change Output would be less than the dust threshold, do nothing.
    if (changeSats < getDustThreshold(changeOutputTemplate)) {
      return this;
    }

    // Add the change output.
    this.transaction.outputs.push({
      lockingBytecode: changeLockingBytecode.bytecode,
      valueSatoshis: changeSats,
    });

    // And append it back to our UTXOs.
    this.change.push({
      outpointTransactionHash: new Uint8Array(),
      outpointIndex: this.transaction.outputs.length - 1,
      sequenceNumber: 0,
      lockingBytecode: changeLockingBytecode.bytecode,
      unlockingBytecode: {
        ...changeTemplate,
        valueSatoshis: changeSats,
        script: 'unlock',
      },
    });

    // Return transaction with Satoshi Change Output.
    return this;
  }

  public generate(): ResolvedTransactionBCH {
    // Generate the transaction
    const generatedTransaction = generateTransaction(this.transaction);

    // Ensure the transaction generated successfully.
    if (!generatedTransaction.success) {
      // lmao but better to show it to help devs.
      const errors = generatedTransaction.errors
        .map((error) => error.errors.map((error) => error.error).join(','))
        .join(',');

      throw new Error(`Failed to generate transaction: ${errors}`);
    }

    return {
      transaction: generatedTransaction.transaction,
      sourceOutputs: this.sourceOutputs,
    };
  }

  public hash(): Uint8Array {
    const txBytes = this.toBytes();

    return hashTransactionUiOrder(txBytes);
  }

  public toBytes(): Uint8Array {
    const generatedTransaction = this.generate();

    return encodeTransactionBCH(generatedTransaction.transaction);
  }

  public toHex(): string {
    return binToHex(this.toBytes());
  }
}
