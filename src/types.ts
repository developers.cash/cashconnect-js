import { Web3WalletTypes } from '@walletconnect/web3wallet';
import type { ProposalTypes, SessionTypes } from '@walletconnect/types';
import type { RpcRequestResponse } from './rpc-types';
import type { WalletTemplate } from '@bitauth/libauth';

export const SupportedChains = ['bch:bchtest', 'bch:bitcoincash', 'bch:bchreg'];

export const SupportedMethods = [
  'wc_authRequest',
  'bch_getBalance_V0',
  'bch_getChangeLockingBytecode_V0',
  'bch_getTokens_V0',
  'bch_signMessage_V0',
  'bch_signTransaction_V0',
];

export const SupportedEvents = ['balancesChanged'];

export interface BchNamespace extends ProposalTypes.BaseRequiredNamespace {
  protocol: 'CashRPC:0';
  template: WalletTemplate;
  allowedTokens: Array<string>;
}

export interface BchSession
  extends Omit<SessionTypes.Struct, 'requiredNamespaces'> {
  requiredNamespaces: {
    bch: BchNamespace;
  };
}

export type BchSessionProposal = Web3WalletTypes.SessionProposal & {
  params: BchSession;
};

export interface WalletProperties {
  autoApprove: Array<string>;
}

export type RequestEvent = Web3WalletTypes.BaseEventArgs<{
  request: {
    method: string;
    /** Params as Extended JSON (i.e. LibAuth's stringify() function) */
    params: string;
  };
}>;

export interface EventCallbacks {
  onSessionsUpdated: (sessions: Record<string, BchSession>) => Promise<void>;

  // These callbacks MUST throw an Error if approval is not granted.
  onSessionProposal: (
    proposalEvent: BchSessionProposal
  ) => Promise<WalletProperties>;

  onSessionDelete: () => Promise<void>;

  onRPCRequest: (
    session: BchSession,
    request: RpcRequestResponse['request'],
    response: RpcRequestResponse['response']
  ) => Promise<void>;

  onError: (error: Error) => Promise<void>;
}

//-----------------------------------------------------------------------------
// RPC Methods
//-----------------------------------------------------------------------------

// TODO: Someone please audit this.
//       The CAIP specs are difficult to interpret.
//       https://specs.walletconnect.com/2.0/specs/clients/auth/data-structures
export interface CacaoPayload {
  aud: string;
  domain: string;
  chainId: string;
  type: 'cashconnect';
  nonce: string;
  iat: string;
}

export interface AuthRequestV0 extends RpcRequestResponse {
  method: 'wc_authRequest';
  params: {
    cacaoPayload: CacaoPayload;
  };
  response: Uint8Array;
}

export interface CacaoAuth {
  cacaoPayload: CacaoPayload;
  publicKey: Uint8Array;
  signature: Uint8Array;
}
