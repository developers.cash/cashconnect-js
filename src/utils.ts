import {
  hexToBin,
  utf8ToBin,
  stringify,
  ripemd160,
  secp256k1,
  sha256,
  encodeCashAddress,
} from '@bitauth/libauth';

export const stringifyExtendedJson = stringify;

export function deriveSandboxKey(
  privateKey: Uint8Array,
  serviceId: string
): Uint8Array {
  const prefix = utf8ToBin('cashconnect:');
  const privateKeyHash = sha256.hash(privateKey);
  const serviceIdHash = sha256.hash(utf8ToBin(serviceId));

  const sandboxedPrivateKey = sha256.hash(
    Uint8Array.from([...prefix, ...privateKeyHash, ...serviceIdHash])
  );

  return sandboxedPrivateKey;
}

export function derivePublicKey(privateKey: Uint8Array): Uint8Array {
  const pubkeyCompressed = secp256k1.derivePublicKeyCompressed(privateKey);

  // If derivation failed...
  if (typeof pubkeyCompressed === 'string') {
    throw new Error(
      'Failed to derive public key: the given Private Key is invalid.'
    );
  }

  return pubkeyCompressed;
}

// TODO: Unused currently. Can consider removal.
export function deriveCashAddr(
  publicKey: Uint8Array,
  network: 'bitcoincash' | 'bchtest' | 'bchreg' = 'bitcoincash'
): string {
  // SHA256 Hash the public key.
  const sha256Hash = sha256.hash(publicKey);

  // RIPEMD160 the SHA256 hash.
  const ripemd160Hash = ripemd160.hash(sha256Hash);

  // Encode as a Cash Address.
  const cashAddr = encodeCashAddress(network, 'p2pkh', ripemd160Hash);

  // Return the account.
  return cashAddr;
}

export function parseExtendedJson(jsonString: string) {
  const uint8ArrayRegex = /^<Uint8Array: 0x(?<hex>[0-9a-f]*)>$/u;
  const bigIntRegex = /^<bigint: (?<bigint>[0-9]*)n>$/;

  return JSON.parse(jsonString, (_key, value) => {
    if (typeof value === 'string') {
      const bigintMatch = value.match(bigIntRegex);
      if (bigintMatch) {
        return BigInt(bigintMatch[1]);
      }
      const uint8ArrayMatch = value.match(uint8ArrayRegex);
      if (uint8ArrayMatch) {
        return hexToBin(uint8ArrayMatch[1]);
      }
    }
    return value;
  });
}

export function getValueByPath(obj: any, path: string): any {
  const pathArray = path.replace(/<|>/g, '').split('.');
  let result = obj;

  for (const key of pathArray) {
    if (result && typeof result === 'object' && key in result) {
      result = result[key];
    } else {
      throw new Error(`${path} does not exist`);
    }
  }

  return result;
}
