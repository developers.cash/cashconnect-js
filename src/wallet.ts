import { CashRPC_V0 } from './rpc';
import { IndexedDbStore } from './store.js';
import {
  deriveSandboxKey,
  derivePublicKey,
  stringifyExtendedJson,
  parseExtendedJson,
} from './utils.js';
import {
  SupportedMethods,
  type RequestEvent,
  type EventCallbacks,
  SupportedChains,
  SupportedEvents,
  BchSession,
  BchNamespace,
  BchSessionProposal,
} from './types.js';

import WalletConnectCore, { Core } from '@walletconnect/core';
import { getSdkError } from '@walletconnect/utils';
import Client, { Web3Wallet, Web3WalletTypes } from '@walletconnect/web3wallet';
import { PairingTypes } from '@walletconnect/types';

import { binToHex, utf8ToBin } from '@bitauth/libauth';
import { CashRPCV0Opts } from './rpc-types';

export class CashConnectWallet {
  // Wallet Connect.
  core: WalletConnectCore;
  web3Wallet!: Client;

  constructor(
    public readonly privateKey: Uint8Array,
    public readonly projectId: string,
    public readonly metadata: Web3WalletTypes.Metadata,
    public readonly eventCallbacks: EventCallbacks,
    public readonly cashRpcOpts: Required<
      Pick<CashRPCV0Opts, 'wallet' | 'network'>
    >
  ) {
    // Instantiate a WC Core with our Project ID.
    // NOTE: We use IndexedDB for storage.
    //       We also use the derived key so that each "account" in a wallet can have a unique WC state.
    //       This is so that we can support multiple accounts running WC concurrently in a Wallet.
    this.core = new Core({
      projectId,
      storage: new IndexedDbStore(
        `cashconnect-${binToHex(derivePublicKey(privateKey))}`
      ),
    });
  }

  async start(): Promise<void> {
    // Initialize our WC Core with the provided metadata.
    this.web3Wallet = await Web3Wallet.init({
      core: this.core,
      metadata: this.metadata,
    });

    // Setup our client callbacks.
    this.web3Wallet.on('session_proposal', (evt) =>
      this.onSessionProposal.bind(this, evt as BchSessionProposal)
    );
    this.web3Wallet.on('session_delete', this.onSessionDelete.bind(this));
    this.web3Wallet.on('session_request', this.onSessionRequest.bind(this));

    // Invoke the callback to let Wallet know that sessions have been updated.
    await this.eventCallbacks.onSessionsUpdated(this.getActiveSessions());
  }

  async pair(url: string): Promise<PairingTypes.Struct> {
    // Convert the URL back into a Wallet-Connect compatible URL.
    // NOTE: CashConnect uses a "cc:" or "web+cc:" (PWA) protocol handler.
    //       This is to differentiate it from other WC implementations.
    const wcUri = url.replace('web+cc:', 'wc:').replace('cc:', 'wc:');

    // TODO: Some funky stuff can happen here if pairing already exists, but session was declined.
    //       Handle it!

    // Pair with the Dapp.
    return await this.core.pairing.pair({ uri: wcUri });
  }

  getActiveSessions(): Record<string, BchSession> {
    return this.web3Wallet.getActiveSessions() as unknown as Record<
      string,
      BchSession
    >;
  }

  async disconnectSession(topic: string): Promise<void> {
    try {
      // Disconnect the session.
      await this.web3Wallet.disconnectSession({
        topic,
        reason: getSdkError('USER_DISCONNECTED'),
      });
    } catch (error) {
      console.error(error);
    } finally {
      // Invoke the callback to let Wallet know that sessions have been updated.
      await this.eventCallbacks.onSessionsUpdated(this.getActiveSessions());
    }
  }

  async disconnectAllSessions(): Promise<void> {
    // Get all active sessions.
    const activeSessions = this.web3Wallet.getActiveSessions();

    // And iterate over them, disconnecting each.
    // NOTE: We disconnect asynchronously so as to not hold things up.
    for (const session of Object.values(activeSessions)) {
      this.disconnectSession(session.topic);
    }
  }

  async walletStateHasChanged(
    chainId: string = 'bch:bitcoincash'
  ): Promise<void> {
    // NOTE: We probably want to use this to handle signTransaction rebuilds too.
    //       See the note under bch_signTransaction RPC handling.

    // Iterate over each of our WC sessions and emit a "balancesChanged" event.
    for (const session of Object.values(this.web3Wallet.getActiveSessions())) {
      // Skip if this WC session does not have "balancesChanged" event in its namespace.
      if (!session.namespaces['bch'].events.includes('balancesChanged')) {
        continue;
      }

      // Instantiate a CashRPC session from the WC session info.
      const sessionBch = session as BchSession;
      const cashRpcSession = this.cashRpcFromSession(sessionBch);

      // Emit the session event.
      this.web3Wallet.emitSessionEvent({
        topic: session.topic,
        event: {
          name: 'balancesChanged',
          balances: await cashRpcSession.getBalance(),
        },
        chainId,
      });
    }
  }

  //-----------------------------------------------------------------------------
  // Wallet Connect Hooks
  //-----------------------------------------------------------------------------

  async onSessionProposal(proposalEvent: BchSessionProposal): Promise<void> {
    try {
      // Check if this proposal is for BCH blockchain.
      if (!proposalEvent.params.requiredNamespaces.bch) {
        throw new Error('requiredNamespaces must contain "bch" key.');
      }

      // Cast required namespaces to our BCH Namespace.
      const requiredNamespace = proposalEvent.params.requiredNamespaces
        .bch as BchNamespace;

      // Ensure that the user has selected a chain.
      if (!requiredNamespace.chains?.length) {
        throw new Error(
          'You must pass in a chain to use ("bitcoincash", "bchtest" or "bchreg")'
        );
      }

      // Get the primary chain to use.
      const [chain, network] = requiredNamespace.chains[0].split(':');

      // Ensure that the chain is BCH.
      if (chain !== 'bch') {
        throw new Error(`Chain "(${chain})" is not supported: Must be "bch"`);
      }

      // Ensure that the chain is supported.
      if (
        network !== 'bitcoincash' &&
        network !== 'bchtest' &&
        network !== 'bchreg'
      ) {
        throw new Error(
          `Network (${network}) is not supported: Must be "bitcoincash", "bchtest" or "bchreg"`
        );
      }

      // Ensure that the protocol version is supported.
      // TODO: Do properly so that we check versions.
      /*
      if (!requiredNamespace.protocol.startsWith('CashRPC')) {
        throw new Error(
          `Protocol "${requiredNamespace.protocol}" is not supported.`
        );
      }
      */

      // Show UI to get approval for the session.
      const walletProperties = await this.eventCallbacks.onSessionProposal(
        proposalEvent
      );

      // Derive the Sandboxed Private Key.
      const sandboxedPrivateKey = deriveSandboxKey(
        this.privateKey,
        proposalEvent.params.proposer.metadata.url
      );

      // Derive the Sandboxed Public Key.
      const sandboxedPublicKey = derivePublicKey(sandboxedPrivateKey);

      // Define our namespaces.
      const finalNamespace = {
        bch: {
          chains: SupportedChains,
          methods: SupportedMethods,
          events: SupportedEvents,
          accounts: [`${chain}:${network}:${binToHex(sandboxedPublicKey)}`],
          // Add any auto-approved methods (returned by the Wallet Event callback) to the namespace.
          autoApprove: walletProperties.autoApprove || ([] as Array<string>),
        },
      };

      // Send a response letting consuming app know the session is approved.
      await this.web3Wallet.approveSession({
        id: proposalEvent.id,
        namespaces: finalNamespace,
      });

      // Invoke the callback to let Wallet know that sessions have been updated.
      this.eventCallbacks.onSessionsUpdated(this.getActiveSessions());
    } catch (error: unknown) {
      // Log the error to the console.
      console.error(error);

      if (error instanceof Error) {
        this.eventCallbacks.onError(error);
      }

      // Send a response letting the consuming app know the session was rejected.
      await this.web3Wallet.rejectSession({
        id: proposalEvent.id,
        reason: {
          code: 5000,
          message: 'User rejected',
        },
      });
    }
  }

  async onSessionDelete(
    deleteEvent: Omit<Web3WalletTypes.BaseEventArgs, 'params'>
  ): Promise<void> {
    try {
      // Trigger UI callback so that frontend can update.
      await this.eventCallbacks.onSessionDelete();

      console.log(deleteEvent);
    } catch (error: unknown) {
      console.log(error);
    } finally {
      await this.eventCallbacks.onSessionsUpdated(this.getActiveSessions());
    }
  }

  async onSessionRequest(requestEvent: RequestEvent): Promise<void> {
    // Extract parameters from request.
    const { topic, id, params } = requestEvent;

    // Wrap in a try/catch.
    // If anything fails or the request is declined, we want to respond with the reason.
    try {
      // Get the session associated with this topic and make sure it exists.
      const session = this.getActiveSessions()[topic] as unknown as BchSession;
      if (!session) {
        throw new Error(
          'Failed to handle request: No session active for the given topic'
        );
      }

      // Extract the method and check if it is allowed.
      // TODO: Check if we need this - WC might do it for us.
      const method = params.request.method;
      CashConnectWallet.checkMethodIsAllowed(session, method);

      // Parse the params from extended JSON into a JS object.
      const parsedParams = parseExtendedJson(params.request.params || '{}');

      // Instantiate a CashRPC session for this request.
      const cashRpcSession = this.cashRpcFromSession(session);

      // Execute the request.
      const handleRequest = async () => {
        switch (method) {
          case 'wc_authRequest': {
            // TODO: We definitely need to add some checks/validations here.
            //       Particularly as it relates to: aud, iat, domain.
            const message = utf8ToBin(
              JSON.stringify(parsedParams.cacaoPayload)
            );
            const response = await cashRpcSession.signMessage({ message });
            return response;
          }
          case 'bch_getBalance_V0':
            return await cashRpcSession.getBalance();
          case 'bch_getChangeLockingBytecode_V0':
            return await cashRpcSession.getChangeLockingBytecode();
          case 'bch_getTokens_V0':
            return await cashRpcSession.getTokens();
          case 'bch_signMessage_V0':
            return await cashRpcSession.signMessage(parsedParams);
          case 'bch_signTransaction_V0':
            // TODO: This needs to function differently:
            //       We can only process one signTransaction request at a time.
            //       The reason for this is that Wallet State (e.g. UTXOs) will most likely change when a request is accepted.
            //       So, we need to somehow "queue" these and make sure they execute sequentially.
            //       Or, maybe "re-execute" them upon Balance Changed?
            //       The latter is probably more reliable - there will be some delay between Wallet Response -> Dapp broadcasting.
            return await cashRpcSession.signTransaction(parsedParams);
          default:
            throw new Error(`Unsupported method called: ${method}`);
        }
      };

      // Execute the request.
      const response = await handleRequest();

      // Request approval for this request.
      await this.eventCallbacks.onRPCRequest(
        session,
        { method, params: parsedParams },
        response
      );

      // Send the response back to the consuming app.
      await this.web3Wallet.respondSessionRequest({
        topic,
        response: {
          id,
          result: stringifyExtendedJson(response),
          jsonrpc: '2.0',
        },
      });
    } catch (error: unknown) {
      // Log the error to the console.
      console.error(error);

      // Respond with error.
      await this.web3Wallet.respondSessionRequest({
        topic,
        response: {
          id,
          jsonrpc: '2.0',
          error: {
            code: 5001,
            message: 'User rejected',
          },
        },
      });

      // Invoke the onError callback.
      if (error instanceof Error) {
        this.eventCallbacks.onError(error);
      }
    }
  }

  private cashRpcFromSession(session: BchSession): CashRPC_V0 {
    // Derive the Sandboxed Private Key.
    const sandboxedPrivateKey = deriveSandboxKey(
      this.privateKey,
      session.peer.metadata.url
    );

    // Create an RPC instance, passing in the signer key and template used for this session.
    const cashRpcSession = new CashRPC_V0({
      network: this.cashRpcOpts.network,
      scope: {
        allowedTokens: session.requiredNamespaces.bch.allowedTokens,
        template: session.requiredNamespaces.bch.template,
      },
      signerKey: sandboxedPrivateKey,
      wallet: this.cashRpcOpts.wallet,
    });

    // Return the CashRPC Session.
    return cashRpcSession;
  }

  //-----------------------------------------------------------------------------
  // Statics
  //-----------------------------------------------------------------------------

  public static checkMethodIsAllowed(session: BchSession, method: string) {
    if (!session.requiredNamespaces.bch.methods.includes(method)) {
      throw new Error(`Unauthorized Method: ${method}`);
    }
  }

  public static async deleteAccount(privateKey: Uint8Array): Promise<void> {
    await IndexedDbStore.deleteDb(`wc-${binToHex(privateKey)}`);
  }
}
